# Abstraction

A simple Dockerized web server for image abstraction (cartoonification effect).

## Quickstart

* Build: `docker build . -t abstraction-server`
* Run: `docker run -d -p 5000:5000 abstraction-server`
* Go to 127.0.0.1:5000 in web browser
* Browse for image to upload
* Abstract!

## References

"Image Smoothing via L0 Gradient Minimization"
Li Xu, Cewu Lu, Yi Xu, Jiaya Jia
ACM Transactions on Graphics, Vol. 30, No. 5 (SIGGRAPH Asia 2011), Dec 2011

