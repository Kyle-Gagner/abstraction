FROM ubuntu:focal AS fftw-builder
RUN apt update
RUN apt install -y wget file gfortran gcc cmake
WORKDIR /root
RUN wget http://fftw.org/fftw-3.3.8.tar.gz -qO- | tar -xzf-
WORKDIR /root/fftw-3.3.8
RUN ./configure --enable-threads --enable-shared --enable-sse2 --enable-avx --enable-avx2 --enable-avx512
RUN make

FROM ubuntu:focal AS abstraction-builder
RUN apt update
RUN apt install -y gcc
COPY --from=fftw-builder /root/fftw-3.3.8/threads/.libs/libfftw3_threads.so* /root/fftw-3.3.8/.libs/libfftw3.so* /root/fftw/
COPY --from=fftw-builder /root/fftw-3.3.8/api/*.h /root/fftw/
COPY ./native/abstraction.c /root
WORKDIR /root
RUN gcc abstraction.c -shared -fPIC -Wall -o libabstraction.so -L/root/fftw/ -I/root/fftw/ -lfftw3 -lfftw3_threads

FROM ubuntu:focal
RUN apt update
RUN apt install -y python3-pip
RUN pip3 install numpy Flask Pillow
COPY --from=abstraction-builder /root/libabstraction.so /root/
COPY --from=fftw-builder /root/fftw-3.3.8/threads/.libs/libfftw3_threads.so* /root/fftw-3.3.8/.libs/libfftw3.so* /root/
COPY ./web/* /root/
WORKDIR /root
ENV LD_LIBRARY_PATH=.
ENTRYPOINT python3 server.py
EXPOSE 5000
