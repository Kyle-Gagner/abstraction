#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <complex.h>
#include <stdio.h>
#include <fftw3.h>
#include <time.h>

int initialized = 0;

#define diff // diff or sobel derivative operators

typedef struct timespec timespec_t;

void ComputeAbstraction(int width, int height, double *img, double start, double stop, double step, double sensitivity, int threads)
{
    int stride = width * 3;
    int sizeplane = width * height;
    int sizeplanes = sizeplane * 3;
    int halfwidth = width / 2 + 1;
    int sizehalfplane = height * halfwidth;
    int sizehalfplanes = sizehalfplane * 3;
    int dims[] = {height, width};
    fftw_iodim iodims[] = {
        (fftw_iodim){.n = height, .is = width, .os = halfwidth},
        (fftw_iodim){.n = width,.is = 1, .os = 1}};
    
    // threading initialization
    if(!initialized)
    {
        fftw_init_threads();
        initialized = 1;
    }

    // set number of threads
    fftw_plan_with_nthreads(threads);

    // start time
    timespec_t tstart;
    clock_gettime(CLOCK_MONOTONIC_RAW, &tstart);

    // buffer for point spread function
    double *psf = fftw_alloc_real(sizeplane);
    // fill in psf
    for(int i = 0; i < sizeplane; i++)
        psf[i] = 0.0;
    for(int y = -2; y < 3; y++)
        for(int x = -2; x < 3; x++)
        {
#ifdef diff
            double l[] = {4.0, -1.0, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0};
#endif
#ifdef sobel
            double l[] = {24.0, 8.0, -4.0, 8.0, 0.0, -4.0, -4.0, -4.0, -2.0};
#endif
            psf[((y + height) % height) * width + ((x + width) % width)] = l[3 * abs(y) + abs(x)];
        }

    // buffers for optical transfer function
    double *otfr = fftw_alloc_real(sizehalfplane);
    double *otfi = fftw_alloc_real(sizehalfplane);
    // compute otf
    fftw_plan psf2otf = fftw_plan_guru_split_dft_r2c(2, iodims, 0, NULL, psf, otfr, otfi, FFTW_ESTIMATE);
    fftw_execute(psf2otf);
    fftw_destroy_plan(psf2otf);
    // free psf buffer
    fftw_free(psf);
    // free imagnary part of otf, zero due to symmetry
    fftw_free(otfi);

    // buffer for current image
    double *curr = fftw_alloc_real(sizeplanes);
    // buffer for fft of current image
    double complex *fftcurr = fftw_alloc_complex(sizehalfplanes);
    // buffer for fft of initial image
    double complex *fftinit = fftw_alloc_complex(sizehalfplanes);
    
    // buffers for horizontal and vertical derivatives
    double *dx = malloc(sizeplanes * sizeof(double));
    double *dy = malloc(sizeplanes * sizeof(double));

    // forward and reverse fft plans
    fftw_plan fwd = fftw_plan_many_dft_r2c(2, dims, 3, curr, NULL, 3, 1, fftcurr, NULL, 3, 1, FFTW_ESTIMATE);
    fftw_plan rev = fftw_plan_many_dft_c2r(2, dims, 3, fftcurr, NULL, 3, 1, curr, NULL, 3, 1, FFTW_ESTIMATE);
    // initialize curr
    memcpy(curr, img, sizeplanes * sizeof(double));
    // compute fftinit
    fftw_execute_dft_r2c(fwd, curr, fftinit);

    // compute lambda
    double lambda = pow(10.0, sensitivity / 10.0);
    
    // main loop
    for(double current = start; current < stop; current += step)
    {
        // compute beta and threshold
        double beta = pow(10.0, current / 10.0);
        double threshold = lambda / beta;

        // compute first derivatives and threshold
        for(int y = 0; y < height; y++)
        {
#ifdef sobel
            int yp = y - 1;
            if(yp == -1) yp = height - 1;
            yp *= stride;
#endif
            int yc = y * stride;
            int yn = y + 1;
            if(yn == height) yn = 0;
            yn *= stride;
            for(int x = 0; x < width; x++)
            {
#ifdef sobel
                int xp = x - 1;
                if(xp == -1) xp = width - 1;
                xp *= 3;
#endif
                int xc = x * 3;
                int xn = x + 1;
                if(xn == width) xn = 0;
                xn *= 3;
                double m = 0.0;
                // computation of first derivative and magnitude
                for(int c = 0; c < 3; c++)
                {
#ifdef diff
                    m += pow(dx[yc + xc + c] = curr[yc + xn + c] - curr[yc + xc + c], 2.0);
                    m += pow(dy[yc + xc + c] = curr[yn + xc + c] - curr[yc + xc + c], 2.0);
#endif
#ifdef sobel
                    m += pow(dx[yc + xc + c] =
                        curr[yp + xn + c] - curr[yp + xp + c] +
                        2.0 * (curr[yc + xn + c] - curr[yc + xp + c]) +
                        curr[yn + xn + c] - curr[yn + xp + c], 2.0);
                    m += pow(dy[yc + xc + c] =
                        curr[yn + xp + c] - curr[yp + xp + c] +
                        2.0 * (curr[yn + xc + c] - curr[yp + xc + c]) +
                        curr[yn + xn + c] - curr[yp + xn + c], 2.0);
#endif
                }
                // thresholding by magnitude
                if(m < threshold)
                    for(int c = 0; c < 3; c++)
                    {
                        dx[yc + xc + c] = 0.0;
                        dy[yc + xc + c] = 0.0;
                    }
            }
        }

        // computation of sum of second derivatives
        for(int y = 0; y < height; y++)
        {
            int yp = y - 1;
            if(yp == -1) yp = height - 1;
            yp *= stride;
            int yc = y * stride;
#ifdef sobel
            int yn = y + 1;
            if(yn == height) yn = 0;
            yn *= stride;
#endif
            for(int x = 0; x < width; x++)
            {
                int xp = x - 1;
                if(xp == -1) xp = width - 1;
                xp *= 3;
                int xc = x * 3;
#ifdef sobel
                int xn = x + 1;
                if(xn == width) xn = 0;
                xn *= 3;
#endif
                for(int c = 0; c < 3; c++)
#ifdef diff
                    curr[yc + xc + c] = dx[yc + xp + c] - dx[yc + xc + c] + dy[yp + xc + c] - dy[yc + xc + c];
#endif
#ifdef sobel
                    curr[yc + xc + c] =
                        dx[yp + xp + c] - dx[yp + xn + c] + dy[yp + xp + c] - dy[yn + xp + c] +
                        2.0 * (dx[yc + xp + c] - dx[yc + xn + c] + dy[yp + xc + c] - dy[yn + xc + c]) +
                        dx[yn + xp + c] - dx[yn + xn + c] + dy[yp + xn + c] - dy[yn + xn + c];
#endif
            }
        }

        // forward transform
        fftw_execute(fwd);
        
        // apply abstraction
        for(int i = 0; i < sizehalfplane; i++)
        {
            int t = i * 3;
            for(int c = 0; c < 3; c++)
            {
                int j = t + c;
                fftcurr[j] = (fftinit[j] + beta * fftcurr[j]) / (1.0 + beta * creal(otfr[i]));
            }
        }
        
        // reverse transform
        fftw_execute(rev);
        for(int i = 0; i < sizeplanes; i++)
            curr[i] /= sizeplane;
    }

    // copy result back to original image buffer
    memcpy(img, curr, sizeplanes * sizeof(double));
    
    // free remaining allocations
    fftw_free(otfr);
    fftw_free(curr);
    fftw_free(fftcurr);
    fftw_free(fftinit);
    free(dx);
    free(dy);

    // report execution time
    timespec_t tstop;
    clock_gettime(CLOCK_MONOTONIC_RAW, &tstop);
    printf("execution time (ms): %lf\n", 1e3 * ((tstop.tv_sec - tstart.tv_sec) + (tstop.tv_nsec - tstart.tv_nsec) * 1e-9));
}