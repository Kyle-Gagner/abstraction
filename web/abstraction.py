﻿from PIL import Image
from os import path, cpu_count
from numpy import array, transpose, float64, uint8, ascontiguousarray
from ctypes import cdll, CFUNCTYPE, POINTER, c_double, c_int
from io import BytesIO
libname = 'libabstraction.so'
libpath = path.dirname(path.abspath(__file__)) + path.sep + libname
lib = cdll.LoadLibrary(libpath)
def abstract(infile, format, start, stop, step, sensitivity):
    img = Image.open(infile)
    order = [0, 1, 2]
    width = img.width
    height = img.height
    arr = ascontiguousarray(transpose(array(img), order).astype(float64) / 255.0)
    del img
    ptr = arr.ctypes.data_as(POINTER(c_double))

    lib.ComputeAbstraction(
        c_int(width), c_int(height), ptr,
        c_double(start), c_double(stop),
        c_double(step), c_double(sensitivity),
        cpu_count())
    order = [order.index(x) for x in range(3)]
    img = Image.fromarray(transpose(arr * 255.0, order).astype(uint8))
    del arr
    with BytesIO() as outfile:
        img.save(outfile, format)
        return outfile.getvalue()
