from flask import Flask, Response, render_template, request
from abstraction import abstract

app = Flask(__name__, template_folder='.')

@app.route("/", methods=['GET', 'POST'])
def main():
    if request.method == 'POST':
        infile = request.files['file']
        start = request.values.get('start', -23, type=float)
        stop = request.values.get('stop', 30, type=float)
        step = request.values.get('step', 1, type=float)
        sensitivity = request.values.get('sensitivity', -20, type=float)
        return Response(abstract(infile, 'jpeg', start, stop, step, sensitivity), mimetype='image/jpeg')
    else:
        return render_template('form.html')

app.run(host='0.0.0.0')